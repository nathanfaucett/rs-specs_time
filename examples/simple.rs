extern crate specs;
extern crate specs_bundler;
extern crate specs_time;

use specs::{DispatcherBuilder, World};
use specs_bundler::Bundler;
use specs_time::{Time, TimeBundle};
use std::{thread, time};

fn main() {
    let mut world = World::empty();

    let mut dispatcher = Bundler::new(&mut world, DispatcherBuilder::new())
        .bundle(TimeBundle::<f64>::default())
        .unwrap()
        .build();

    for _ in 0..60 {
        dispatcher.dispatch(&world);
        thread::sleep(time::Duration::from_millis(16));
    }

    let time = world.fetch::<Time<f64>>();
    println!("{:?}", *time);
}
